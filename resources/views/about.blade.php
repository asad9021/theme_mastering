@extends('layouts.layout')
@section('pageHeader','About Page')

@section('subTitle','Its for the about page')
@section('img_link')
    <header class="intro-header" style="background-image: url({!! asset('assets/img/about-bg.jpg') !!})">
        @endsection