@extends('layouts.layout')
@section('pageHeader','Contact Page')


@section('subTitle','Its for the contact page')

@section('img_link')
    <header class="intro-header" style="background-image: url({!! asset('assets/img/contact-bg.jpg') !!})">
@endsection