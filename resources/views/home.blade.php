@extends('layouts.layout')
@section('pageHeader','Home')
@section('img_link')
    <header class="intro-header" style="background-image: url({!! asset('assets/img/home-bg.jpg') !!})">
@endsection